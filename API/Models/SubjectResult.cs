namespace API.Models
{
    public class SubjectResult
    {
        public string subject { get; set; }
        public Result[] results { get; set; }
    }
}