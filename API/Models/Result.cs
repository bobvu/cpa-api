namespace API.Models
{
    public class Result
    {
        public int year { get; set; }
        public string grade { get; set; }
    }
}