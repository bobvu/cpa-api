using System.Threading.Tasks;
using API.Models;
using API.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ResultsController : ControllerBase
    {
        private readonly IResultRepository _repo;

        public ResultsController(IResultRepository repo)
        {
            _repo = repo;
        }

        [HttpGet]
        public ActionResult<SubjectResult[]> GetResults()
        {
            var results = _repo.GetResults();
            return Ok(results);
        }
    }
}