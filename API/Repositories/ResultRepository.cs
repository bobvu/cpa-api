using System;
using API.Models;
using System.Collections.Generic;
using System.IO;
using System.Text.Json;
using Microsoft.Extensions.Logging;
using API.Exceptions;
using System.Net;
using API.Services;

namespace API.Repositories
{
    public class ResultRepository : IResultRepository
    {
        private IFileService _fileService;

        public ResultRepository(IFileService fileService)
        {
            _fileService = fileService;

        }

        public SubjectResult[] GetResults()
        {
            return _fileService.GetData();
        }
    }
}