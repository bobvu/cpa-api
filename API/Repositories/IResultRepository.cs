using API.Models;
namespace API.Repositories
{
    public interface IResultRepository
    {
        SubjectResult[] GetResults();
    }
}