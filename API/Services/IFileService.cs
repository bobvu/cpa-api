using API.Models;

namespace API.Services
{
    public interface IFileService
    {
        SubjectResult[] GetData();
    }
}