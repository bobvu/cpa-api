using System;
using System.IO;
using System.Net;
using System.Text.Json;
using API.Exceptions;
using API.Models;

namespace API.Services
{
    public class FileService : IFileService
    {
        private string _dataSource;

        public FileService(string dataSource = @"../API/Data/results.json")
        {
            _dataSource = dataSource;
        }

        public SubjectResult[] GetData()
        {

            string resultData = "[]";
            if (!File.Exists(_dataSource))
            {
                throw new IOException("File not found");
            }
            resultData = File.ReadAllText(_dataSource);
            var results = JsonSerializer.Deserialize<SubjectResult[]>(resultData);
            if (results != null && results.Length > 0)
            {
                return results;
            }
            throw new RestException(HttpStatusCode.NotFound, "No results found");


        }
    }
}