# Cpa-Api

This project was generated with [dotnet CLI] version 3.1.200

## Prerequsites

- Need to have `donet core 3.1` installed in your machine.

## Make it run

- Clone the repo to your local machine: `git clone https://bobvu@bitbucket.org/bobvu/cpa-api.git`
- Open Terminal (Mac) / Cmd (Windows) at the source code folder (`cpa-api`)
- To start up the API:
  - cd to `/API` folder,
  - run `dotnet restore`
  - then `dotnet run`, the api will run at `https://localhost:5001`
- To run unit tests
  - cd to the root folder `cpa-api`, then cd to `Tests`folder
  - run `dotnet test`
