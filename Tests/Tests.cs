using System.Net;
using System;
using System.IO;
using API.Exceptions;
using API.Models;
using API.Services;
using Xunit;
using System.Text.Json;

namespace Tests
{
    public class Tests
    {

        protected readonly string exactPath = @"../../../Data/results.json";
        protected readonly string wrongPath = @"../../../Data/results1.json";
        protected readonly string emptyFile = @"../../../Data/empty.json";
        [Fact]
        public void WhenFilePathIsWrongOrFileNotFound_ReaderShouldRaise_IOException()
        {
            Assert.Throws<IOException>(() => new FileService(wrongPath).GetData());

        }

        [Fact]
        public void WhenFilePathIsRight_ShouldReturn_CorrectResults()
        {

            var results = new FileService(exactPath).GetData();

            Assert.IsType<SubjectResult[]>(results);
            Assert.Equal(5, results.Length);
            Assert.Equal("Strategic Management Accounting", results[0].subject);
            Assert.Equal("FAIL", results[0].results[0].grade);
            Assert.Equal(2015, results[0].results[0].year);

        }

        [Fact]
        public void When_results_are_empty_ShouldRaise_RestException_NotFound()
        {

            var exception = Record.Exception(() => new FileService(emptyFile).GetData());
            Assert.IsType<RestException>(exception);
            Assert.Equal("No results found", (exception as RestException).Errors);
            Assert.Equal(HttpStatusCode.NotFound, ((exception as RestException).Code));

        }
    }
}
